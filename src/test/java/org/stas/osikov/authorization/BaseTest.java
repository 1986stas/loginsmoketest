package org.stas.osikov.authorization;

import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.selenide.AllureSelenide;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.stas.osikov.authorization.environment.Browser;
import org.stas.osikov.authorization.listeners.AllureAttachmentListener;
import org.stas.osikov.authorization.page.AbstractPage;
import org.stas.osikov.authorization.page.LoginPage;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;

import static com.codeborne.selenide.Selenide.page;

@Listeners(AllureAttachmentListener.class)
@ContextConfiguration(classes = AbstractPage.class)
public class BaseTest extends AbstractTestNGSpringContextTests {

    @Autowired
    LoginPage loginPage;

    @BeforeClass(alwaysRun = true)
    public void openPage() {
        SelenideLogger.addListener("AllureSelenide", new AllureSelenide().
                screenshots(true).savePageSource(true));
        Browser.getInstance();
    }

    protected <T> T at(Class<T> pageClass){
        return page(pageClass);
    }
}