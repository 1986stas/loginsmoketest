package org.stas.osikov.authorization.dataProvider;

import org.stas.osikov.authorization.pojo.User;
import org.testng.annotations.DataProvider;

public class LoginDataProvider {

    @DataProvider(name = "getUserData", parallel = true)
    public Object[][] getUserCredentials(){
        User[][] user = new User[4][1];
        user[0][0] = new User("", "Stas1234", false);
        user[1][0] = new User("osikov@ukr.net", "", false);
        user[2][0] = new User("", "", false);
        user[3][0] = new User("osikov@ukr.net", "Stas1234", true);
        return user;
    }
}
