package org.stas.osikov.authorization.listeners;

import com.codeborne.selenide.WebDriverRunner;
import io.qameta.allure.Attachment;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.stas.osikov.authorization.logger.CustomLogger;
import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

public class AllureAttachmentListener extends TestListenerAdapter implements IRetryAnalyzer {

    private final CustomLogger LOGGER = CustomLogger.getInstance();

    private int count = 0;
    private static final int MAX_TRY = 2;

    @Override
    public void onTestStart(ITestResult result) {
        LOGGER.info("Test class started: " + result.getTestClass().getName());
        LOGGER.info("Test started: " + result.getName());
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        makeScreenshot();
        LOGGER.info("Test SUCCESS: " + result.getName());
    }

    @Override
    public void onTestFailure(ITestResult result) {
        makeScreenshot();
        LOGGER.info("Test FAILED: " + result.getName());
        if (result.getThrowable() != null) {
            result.getThrowable().printStackTrace();
        }
    }

    @Override
    public void onTestSkipped(ITestResult testResult) {
        LOGGER.info("Test SKIPPED: " + testResult.getTestClass().getName());
    }

    @Attachment(value = "Page screenshot", type = "image/png")
    private byte[] makeScreenshot() {
        return ((TakesScreenshot) WebDriverRunner.getWebDriver()).getScreenshotAs(OutputType.BYTES);
    }

    @Override
    public boolean retry(ITestResult iTestResult) {
        if (!iTestResult.isSuccess()) {
            if (count < MAX_TRY) {
                count++;
                iTestResult.setStatus(ITestResult.FAILURE);
                return true;
            } else {
                iTestResult.setStatus(ITestResult.FAILURE);
            }
        } else {
            iTestResult.setStatus(ITestResult.SUCCESS);
        }
        return false;
    }
}
