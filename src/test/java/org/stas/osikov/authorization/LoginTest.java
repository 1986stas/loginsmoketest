package org.stas.osikov.authorization;

import io.qameta.allure.*;
import org.stas.osikov.authorization.dataProvider.LoginDataProvider;
import org.stas.osikov.authorization.page.AccountPage;
import org.stas.osikov.authorization.pojo.User;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

@Epic(value = "Smoke test of Login page")
@Feature(value = "Here we check all critical scenarios of user login")
public class LoginTest extends BaseTest {

    @BeforeMethod
    public void setUp(){
        loginPage.openLoginPage();
    }

    @SuppressWarnings("TestDataSupplier")
    @Test(dataProvider = "getUserData", dataProviderClass = LoginDataProvider.class)
    @Severity(SeverityLevel.NORMAL)
    @Description("In this test we're going to check bad and good user credentials, eventually, user should be able to login with valid password and email")
    public void userShouldBeLoggedIn(User user){
        assertThat("Forgot password link has incorrect text", loginPage.getTextForgotPasswordLink(), is(equalTo("I forgot my password")));
        assertThat("Continue button is not displayed", loginPage.isContinueButtonDisplayed());
        assertThat("Email form is not displayed", loginPage.isEmailFormDisplayed());
        assertThat("Password form is not displayed", loginPage.isPasswordFormDisplayed());
        loginPage.enterCredentials(user).
                    clickOnContinueButton();
        assertThat(at(AccountPage.class).isAccountPageTitleDisplayed(user.isLoggedIn()), is(equalTo(user.isLoggedIn())));
    }
}
