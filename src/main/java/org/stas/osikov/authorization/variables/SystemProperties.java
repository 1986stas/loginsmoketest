package org.stas.osikov.authorization.variables;

import org.aeonbits.owner.Config;

import static org.stas.osikov.authorization.variables.PropertyConstants.CONFIG;

@Config.Sources(value = {"classpath:" + CONFIG})
public interface SystemProperties extends Config {

    @Key("browser")
    @DefaultValue("chrome")
    String browser();

    @Key("url")
    @DefaultValue("https://my.exness.com/accounts/sign-in?force=true")
    String url();

    @Key("browser_size")
    @DefaultValue("1920x1080x24")
    String browserSize();

    @Key("headless")
    @DefaultValue("true")
    Boolean isHeadless();

    @Key("condition.timeout")
    @DefaultValue("15")
    Long conditionTimeout();

    @Key("timeout")
    @DefaultValue("10")
    Long timeout();
}
