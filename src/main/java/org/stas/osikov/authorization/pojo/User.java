package org.stas.osikov.authorization.pojo;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
@AllArgsConstructor
public class User {
    private String email;
    private String password;
    private boolean isLoggedIn;
}
