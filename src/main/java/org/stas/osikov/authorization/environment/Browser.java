package org.stas.osikov.authorization.environment;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import io.github.bonigarcia.wdm.WebDriverManager;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.aeonbits.owner.ConfigFactory;
import org.openqa.selenium.WebDriver;
import org.stas.osikov.authorization.logger.CustomLogger;
import org.stas.osikov.authorization.variables.SystemProperties;

import javax.naming.NamingException;

@SuppressWarnings("all")
public final class Browser {

    private static final CustomLogger LOGGER = CustomLogger.getInstance();

    private static BrowserFactory instance;
    private final static SystemProperties systemProperties = ConfigFactory.create(SystemProperties.class);

    private static Browsers currentBrowser = Browsers.valueOf((System.getenv("browser") == null
            ? systemProperties.browser()
            : System.getenv("browser")).toUpperCase());
    static final boolean IS_HEADLESS = systemProperties.isHeadless();
    public static final String BROWSER_URL = systemProperties.url();
    public static final long CONDITION_WAIT = systemProperties.conditionTimeout();
    public static final long TIMEOUT = systemProperties.timeout();
    private static final String BROWSER_SIZE = systemProperties.browserSize();

    private Browser(){
        LOGGER.info(String.format("browser %s is ready", currentBrowser.name()));
    }

    public static void getInstance() {
        if (instance == null) {
            synchronized (BrowserFactory.class) {
                initNewDriver();
                instance = new BrowserFactory();
            }
        }
    }

    static boolean isGrid() {
        return false;
    }

    public static WebDriver getDriver() {
        return WebDriverRunner.getWebDriver();
    }

    private static void initNewDriver() {
        Configuration.timeout = 5000;
        WebDriverManager.chromedriver().version("80");
        Configuration.pollingInterval = 5000;
        Configuration.browserSize = BROWSER_SIZE;
        try {
            BrowserFactory.setUp(currentBrowser);
            WebDriverRunner.addListener(new WebEventListener());
        } catch (NamingException e) {
            LOGGER.info("Browser: getting New Driver " + e.getExplanation());;
        }
    }

    @AllArgsConstructor(access = AccessLevel.PROTECTED)
    public enum Browsers {
        FIREFOX("firefox"),
        CHROME("chrome");

        @Getter
        private final String value;
    }

}
