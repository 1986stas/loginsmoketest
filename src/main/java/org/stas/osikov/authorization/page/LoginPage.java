package org.stas.osikov.authorization.page;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.springframework.stereotype.Component;
import org.stas.osikov.authorization.pojo.User;

import static com.codeborne.selenide.Selenide.$;

@Component
public class LoginPage extends MainPage {

    private final static String EMAIL_FORM_XPATH = "//form/descendant-or-self::input[@id = 'login']";
    private final static String PASSWORD_FORM_XPATH = "//form/descendant-or-self::input[@id = 'password']";
    private final static String CONTINUE_BUTTON_XPATH = "//form/descendant-or-self::button[@type = 'submit']";
    private final static String FORGOT_PASSWORD_XPATH = "//signin-form/descendant-or-self::span/a";

    private final SelenideElement emailForm = $(By.xpath(EMAIL_FORM_XPATH));
    private final SelenideElement passwordForm = $(By.xpath(PASSWORD_FORM_XPATH));
    private final SelenideElement continueButton = $(By.xpath(CONTINUE_BUTTON_XPATH));
    private final SelenideElement forgotPasswordLink = $(By.xpath(FORGOT_PASSWORD_XPATH));


    @Step
    public LoginPage openLoginPage(){
        openPage(LoginPage.class);
        waitForPageLoad();
        waitForLoginPageElements();
        return this;
    }

    @Step
    public boolean isContinueButtonDisplayed(){
        return isElementVisible(continueButton);
    }

    @Step
    public boolean isEmailFormDisplayed(){
        return isElementVisible(emailForm);
    }

    @Step
    public boolean isPasswordFormDisplayed(){
        return isElementVisible(passwordForm);
    }

    @Step
    public String getTextForgotPasswordLink(){
        return readText(forgotPasswordLink);
    }

    @Step
    public LoginPage enterCredentials(User user){
        writeText(emailForm, user.getEmail());
        writeText(passwordForm, user.getPassword());
        return this;
    }

    @Step
    public LoginPage clickOnContinueButton(){
        click(continueButton);
        return this;
    }

    private void waitForLoginPageElements(){
        waitForElementToBeExist(EMAIL_FORM_XPATH);
        waitForElementToBeExist(PASSWORD_FORM_XPATH);
        waitForElementToBeExist(FORGOT_PASSWORD_XPATH);
        waitForElementToBeExist(CONTINUE_BUTTON_XPATH);
    }
}
