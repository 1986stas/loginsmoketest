package org.stas.osikov.authorization.page;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import lombok.Synchronized;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.stas.osikov.authorization.environment.Browser;

import static com.codeborne.selenide.Selenide.open;

@SuppressWarnings("rawtypes")
public abstract class MainPage<Page extends MainPage> {
    private final static long TIME_TO_WAIT = Browser.TIMEOUT;
    private static final String BASE_URL = Browser.BROWSER_URL;

    <T extends ElementsCollection> void selectItem(String item, T t) {
        t.stream()
                .filter(selenideElement -> readText(selenideElement).contains(item))
                .forEach(selenideElement -> selenideElement.waitUntil(Condition.visible, TIME_TO_WAIT)
                        .hover().click());
    }

    @Synchronized
    protected void openPage(Class<Page> pageClass){
        open(BASE_URL, pageClass);
    }

    /**
     * methods below can be improved by switching between By or WebElement, or any other Remote WebElement
     * @param elementAttr
     * @param <V>
     */

    protected <V> void click(V elementAttr) {
        ((SelenideElement) elementAttr).waitUntil(Condition.appear, TIME_TO_WAIT).click();
    }

    protected <V> void writeText(V elementAttr, String text) {
        ((SelenideElement) elementAttr).waitUntil(Condition.appear, TIME_TO_WAIT).setValue(text);
    }

    protected <V> boolean isElementVisible(V elementAttr) {
        return  ((SelenideElement) elementAttr).is(Condition.visible);
    }

    protected <V> String readText(V elementAttr) {
        return ((SelenideElement) elementAttr).text();
    }

    protected void waitForPageLoad() {
        SmartWait.waitForJSandJQueryToLoad();
    }

    protected WebElement waitForElementToBeExist(String locator) {
        return SmartWait.waitFor(ExpectedConditions.visibilityOfElementLocated(By.xpath(locator)), Browser.TIMEOUT);
    }
}