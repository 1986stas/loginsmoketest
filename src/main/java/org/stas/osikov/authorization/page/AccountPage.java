package org.stas.osikov.authorization.page;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.springframework.stereotype.Component;

import static com.codeborne.selenide.Selenide.$;

@SuppressWarnings("rawtypes")
@Component
public class AccountPage extends MainPage {

    private final static String ACCOUNT_TITLE_XPATH = "//h1[contains (@class, 'AccountsPage')]";

    @Step
    public boolean isAccountPageTitleDisplayed(boolean waitForElement){
        if (waitForElement){
            waitForElementToBeExist(ACCOUNT_TITLE_XPATH);
        }
        return isElementVisible($(By.xpath(ACCOUNT_TITLE_XPATH)));
    }
}
