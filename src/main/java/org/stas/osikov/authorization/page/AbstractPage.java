package org.stas.osikov.authorization.page;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "org.stas.osikov.authorization")
public class AbstractPage {
}
