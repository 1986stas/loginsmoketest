package org.stas.osikov.authorization.exception;

public class UserNotFoundException extends Exception {

    public UserNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

}
