package org.stas.osikov.authorization.logger;

import lombok.NoArgsConstructor;
import lombok.Synchronized;
import lombok.extern.log4j.Log4j2;

import java.io.PrintWriter;
import java.io.StringWriter;

@Log4j2
@NoArgsConstructor
public class CustomLogger {

    private static CustomLogger instance = null;

    @Synchronized
    public static CustomLogger getInstance() {
        if (instance == null) {
            instance = new CustomLogger();
        }
        return instance;
    }

    private void printStackTrace(Throwable e) {

        StringWriter stringWriter = new StringWriter();
        e.printStackTrace(new PrintWriter(stringWriter));
        log.warn(e.getMessage());
        log.warn(stringWriter.toString());
    }

    public void printStackTrace(Throwable e, String message) {
        log.warn(message);
        printStackTrace(e);
    }

    public void step(final int step, final String description) {
        logStepMsg(String.format("%1$s. %2$s", step, description));
    }

    public void debug(Object message) {
        log.debug(message);
    }


    public void error(Object message) {
        log.error(message);
    }

    public void warn(final String message) {
        log.warn(message);
    }

    public void info(Object message) {
        log.info(message);
    }

    public void fatal(final String message) {
        log.fatal(message);
    }

    public void debug(Object message, Throwable throwable) {
        log.debug(message, throwable);
    }

    private void logStepMsg(final String msg) {
        info(String.format("--------==[ %1$s.]==--------", msg));
    }
}
